---
title: "Is it possible to use machine learning to predict readmission rates of diabetis patient based on age, gender,race and payercode?"
author: "Rik van de Pol"
date: "9/17/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Used libraries
```{r, message=FALSE}
library("ggplot2")
library("tidyr")
library("dplyr")
library("knitr")
library("ggfortify")
library("dummies")
library("reshape")
library("plyr")
library("scales")
library("kableExtra")
library("ggcorrplot")
library("foreign")
library("rockchalk")
```

## R Markdown

# Log
On september 10th 2019, the first lesson of this project took place from 12:30 to 16:00. 
The data of the following research paper has been chosen for this: Project 6 [Predicting hospital readmission of diabetes patients](https://archive.ics.uci.edu/ml/datasets/Diabetes+130-US+hospitals+for+years+1999-2008).

On the 12th of september the research question has been formulated, and some basic work preperation.

On september 17th the summary of the log was written, and the possible ethical dillemma's of the research question have been discussed. In addition a new column has been added to the dataset explaining each attribute. It now also contains the name used in the diabetic_data.csv dataset. Furthermore, in order to create boxplots, some of the data has been transformed so it can easily be used.

On september 23rd, dummy data of the attributes mentioned in the research question has been created.

On september 24th one set of the dummy data has been used to create a heatmap, though this one is still unclear. The modifications for the race attribute have been added. The table containing the data information has been modified and fitted to a single page.

On september 25th the majority of the plots have been finished. For each attribute mentioned in the research quesion a barplot has been created.

On september 26th new plots where created, these include the boxplots, heatmap and contingency tables. Work was done from 08:30-18:00.

On october 1st the work on machine learning was started. All the dummy data has been made and prepared to be used in weka.

Oktober 9th several models have been created using the weka explorer. The data used is the raw data, with some attributes removed. It was transformed using the NominalToBinary filter available in weka. A confusion matrix has been created using created dummy data.

On oktober 16th work on the javawrapper has started. Several things have been done, but shadowjar does not work yet.

On oktober 18th shadowjar now works and the program runs, but doesn't do anything yet,

Oktober 18th work has been performed on the wrapper program. It is now possible provide the program with a file and the program will perform several checks to see if the file is what is expected.

From this point on I have neglected to keep track of what I did every day, because I was working on it nearly every day with the exception of week 45 and 46. During this time the wrapper has been completed. It is now possible to provide the program with a single instance, which will then be classified. It will also write the newly classified instance(s) to a file, so that the user can easily read said file to view the results. It will also print the model statistics.



# Possible ethical issues
The goal of this project is to research whether a patients race, gender and payer code of diabetis patients have an influence on the patients readmission rates to the hospital.
This will be researched because the it is interesting to know if one of the previously mentioned attributes can affect readmission rates. It is also interesting to see which group has the most diabetis patients. This kind of research can be rather problematic due to ethical difficulties, as some people may find that using attributes such as race, gender and wealth can be rascist, sexist or classism. This paper distances itself from any prejudice, and serves only to perform research from a scientific standpoint.


# Summary
The data from the article 'Impact of HbA1c Measurement on Hospital Readmission Rates: Analysis of 70,000 Clinical Database Patient Records' consists of patient data of 70.000 admitted patient who where diagnosed with diabetes. The article describes a of 50 attributes, such as race, gender, weight and payer code.

The articale concluded that the decision to obtain a measurement of HbA1c for patients with diabetes mellitus is a useful predictor of readmission rates which may prove valuable in the development of strategies to reduce readmission rates and costs for the care of individuals with diabetes mellitus.

The attributes are explained in detail [**here**](https://www.hindawi.com/journals/bmri/2014/781670/tab1/).

# Figures

```{r}
#Create diabetis.variable that contains the variable values
diabetes.data <- read.csv('csvfiles/diabetic_data.csv', header = TRUE, sep = ',')
diabetes.attribute.data <- read.csv('csvfiles/Attributes.csv', header = TRUE, sep = '\t')
```


```{r, fig.cap="Boxplots of all age groups and their time spend in the hospital"}
ggplot2::ggplot(diabetes.data, aes(x=age, y=time_in_hospital)) + 
  geom_boxplot(fill='steelblue', color="black") +
  labs(x = 'Age groups', y = 'Time in days')
```
The barplots above show time spend in the hospital for each age group. It is clear to see that the age group of 0-10 spends the least amount of time in the hospital, while the group 80-90 and 90-100 years spends the most time in the hospital. In the 50-60,60-70 and 70-80 years old the variation is the largest.


```{r, fig.cap= "Boxplots of all age groups in years and the number of diagnoses needed"}
ggplot2::ggplot(diabetes.data, aes(x=age, y=number_diagnoses)) +
  geom_boxplot(fill='steelblue', color="black") +
  labs(title = "Boxplots of all age groups and their time spend in the hospital", x = 'Age groups', y = 'Number of Diagnoses')
```
The boxplots above show the number of diagnoses needed to determine the patient has diabetes. In some cases this can go as high as 16 diagnoses! The 0-10 age group needs the least amount of diagnoses to determine the patient has diabetes. Most of the age groups seem to need a simillar amount of diagnoses before it is determined that the patient has diabetes, the exceptions to this are the 0-10,10,20 and 20-30 age group. The other age groups are very simmilar, their q3 being the same and q1 becoming higher when the age rises.

```{r}
# Create usable age data
set.seed(1)
age.data <- data.frame(age_group = diabetes.data$age)
age.data <- age.data %>%
  dplyr::group_by(age_group) %>%
  dplyr::summarise(num_patients = length(age_group))
```

```{r, fig.cap="Number of patients per age group"}
# Create a bar plot of the age groups, using ggplot2
ggplot2::ggplot(data=age.data, aes(x=age_group, y=num_patients)) +
  geom_bar(stat="identity", fill = 'steelblue') +
  ggtitle("Number of patients per age group") +
  xlab('Age group') +
  ylab('Number of patients') +
  geom_text(aes(label=num_patients), vjust=0, hjust=1.2, color="black", size=3.5) +
  coord_flip()
```
The barplot shown above clearly shows that most patients in the dataset are in the 70-80 age group. This is logical, because no diabetis type is specified. Therefore it's reasonable to assume that these people have type 2 diabetis.
The smallest age group is the 0-10 group. This is also not unexpected, since diabetis usually develops at a later point in life.


```{r}
set.seed(1)
race.data <- data.frame(race = diabetes.data$race)
race.data <- race.data %>%
  dplyr::group_by(race) %>%
  dplyr::summarise(num_each_race = length(race))
```

```{r, fig.cap="Number of people in each ethnicity group"}
# Create a barplot using the previously created dataframe
ggplot2::ggplot(data=race.data, aes(x=race, y=num_each_race)) +
  geom_bar(stat="identity", fill = 'steelblue') +
  xlab('Ethnicity') +
  ylab('Number of people') +
  geom_text(aes(label=num_each_race), vjust=0, hjust=1.2, color="black", size=3.5) +
  coord_flip()
```
The plot shown above shows that most of the patients have caucasian ethnicity. There doesn't seem to be a particular reason for this.


```{r}
# Create dataframe of the payer code attribute
set.seed(1)
payer.codes <- data.frame(payer_code = diabetes.data$payer_code)
payer.code.data <- payer.codes %>% 
  dplyr::group_by(payer_code) %>% 
  dplyr::summarise(num_payer_codes = length(payer_code))
payer.code.data <- as.data.frame(payer.code.data)
```

```{r, fig.cap="Number of people per payer code"}
# Create a barplot using the previously created dataframe
ggplot2::ggplot(data=payer.code.data, aes(x=payer_code, y=num_payer_codes)) +
  geom_bar(stat="identity", fill = 'steelblue') +
  xlab('Payer code') +
  ylab('Number of people') +
  geom_text(aes(label=num_payer_codes), vjust=0, hjust=1.2, color="black", size=3.5) +
  coord_flip()
```
The plot above shows that most of the payer code data is missing. This may be because the patients were unwilling to share this information or,more likely, because they dont have insurance. As mentioned before this dataset was obtianed in the United States of America. In the USA it is not mandatory to have healthcare insurance, and therefore these patients would not be able to provide health care information.


```{r}
# Create a dataframe of all genders and the corresponding counts
set.seed(1)
genders <- data.frame(genders = diabetes.data$gender)
gender.data <- genders %>% 
  dplyr::group_by(genders) %>% 
  dplyr::summarise(num_genders = length(genders))
gender.data <- as.data.frame(gender.data)
```

```{r, fig.cap="Number of people in each gender group"}
# Create a barplot using ggplot2 of the previously created dataframe
ggplot2::ggplot(data=gender.data, aes(x=genders, y=num_genders)) +
  geom_bar(stat="identity", fill = 'steelblue') +
   ggtitle("Amount of each gender") +
  xlab('Genders') +
  ylab('Number of people') +
  geom_text(aes(label=num_genders), vjust=0, hjust=1.2, color="black", size=3.5) +
  coord_flip()
```
The above barplot shows that the majority of the patients are female. Note that three patients have no male or female label. This is possibly because they dont identify as either male or female.


```{r}
set.seed(1)
readmission <- data.frame(readmitted = diabetes.data$readmitted)
readmission.data <- readmission %>% 
  dplyr::group_by(readmitted) %>% 
  dplyr::summarise(num_of_readmission = length(readmitted))
readmission.data <- as.data.frame(readmission.data)
```


```{r, fig.cap="Number of in each readmission group"}
ggplot2::ggplot(data=readmission.data, aes(x=readmitted, y=num_of_readmission)) +
  geom_bar(stat="identity", fill = 'steelblue') +
  ggtitle("Number of readmissions") +
  xlab('Days until readmission') +
  ylab('Number of people') +
  geom_text(aes(label=num_of_readmission), vjust=0, hjust=1.2, color="black", size=3.5) +
  coord_flip()
```
The above barplot shows the total number of readmissions as a function the time untill readmission. It's notable that most people were not readmitted after being discharged. This is good news, as they managed to stay healthy enough. unfortunately a large number of people were readmitted after more than 30 days, and a small number of people were readmitted after less than 30 days.


```{r, fig.cap="Number of people that reveived medicine and whether they had to be readmissioned or not"}
medicine.readmission <- diabetes.data[, 49:50]
med.red<- medicine.readmission %>% 
  dplyr::group_by(diabetesMed, readmitted) %>% 
  dplyr::summarise(nums = length(diabetesMed))
med.red.data <- as.data.frame(med.red)
med.red.data$colours <- as.factor(rep(c("red", "blue"), each = 3))
ggplot(med.red.data, aes(x=readmitted, y=nums)) +
  geom_bar(stat="identity") +
  ggtitle("Stacked barplots of patients that have used medicine, 
          those that did not and when they had to be readmissioned") +
  ylab("Number of patients") +
  geom_col(aes(fill = med.red.data$colours), width = 0.7)+
  geom_text(aes(label=nums), vjust=1.6, hjust=0.5, color="black", size=2.5) +
  scale_fill_discrete(name = "Legend", labels = c("Medicine", "No Medicine"))
```
The table above shows a stacked barplot in which the time until readmission is plotted on the x-axis and the numper of patients in that group on the y-axis. It shows that most people were not readmitted, regardless of they recieved medicine or not.


# Contingency tables
```{r}
gender.readmission.table <- table(diabetes.data$gender, diabetes.data$readmitted)
age.readmission.table <- table(diabetes.data$age, diabetes.data$readmitted)
rownames(age.readmission.table) <- c('0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90', '90-100') 
payercode.readmission.table <- table(diabetes.data$payer_code, diabetes.data$readmitted)
medspec.readmission.table <- table(diabetes.data$medical_specialty, diabetes.data$readmitted)
```

```{r}
knitr::kable(gender.readmission.table, caption = 'Contingency of the genders and readmission groups of the patients', format="latex", booktabs=TRUE)
```

```{r}
knitr::kable(age.readmission.table, caption = 'Contingency of the genders and readmission groups of the patients', format="latex", booktabs=TRUE)
```

```{r, echo=FALSE}
knitr::kable(payercode.readmission.table, caption = 'Contingency of the payer codes and readmission groups of the patients', format="latex", booktabs=TRUE)
```

```{r}
knitr::kable(medspec.readmission.table, caption = 'Contingency of the medical speciality and readmission groups of the patients', format="latex", booktabs=TRUE)
```

```{r, fig.cap="Number of people that per gender in each readmission group"}
gender.readmission.frame <- as.data.frame(gender.readmission.table)
ggplot2::ggplot(gender.readmission.frame, aes(x = Var1, y = Var2)) +
  geom_point(aes(size = Freq)) + 
  labs(title = "Scatter plot of gender vs readmission", x = "Gender", y = "Time to Readmission")
```


```{r, fig.cap="Number of people that per age group in each readmission group"}
age.readmission.frame <- as.data.frame(age.readmission.table)
ggplot2::ggplot(age.readmission.frame, aes(x = Var1, y = Var2)) +
  geom_point(aes(size = Freq)) + 
  labs(title = "Scatter plot of age vs readmission", x = "Age group", y = "Time to Readmission")
```


```{r, fig.cap="Number of people in each readmission group for each payercode"}
payercode.readmission.frame <- as.data.frame(payercode.readmission.table)
ggplot2::ggplot(payercode.readmission.frame, aes(x = Var1, y = Var2)) +
  geom_point(aes(size = Freq)) + 
  labs(title = "Scatter plot of healthcare insurance vs readmission", x = "Healthcare insurer", y = "Time to Readmission")
```

The plot shown in the above figure shows the number of people in each readmission group for each payer code.
This shows that people with an unknown health care provider ware rarely readmissioned, the same goes for MC.
Other then these two the readmission rates seem to be fairly well distributed.



```{r, "Heatmap showing the correlation of number of diagnoses, number of labprocedures and time spent in hospital"}
pre.heatmap <- cor(diabetes.data[c(10,13,22)])
ggcorrplot::ggcorrplot(pre.heatmap, lab = TRUE, title = "Heatmap showing the correlation between 
                       the listen attributes")
```
A simple heatmap containing the only three numeric data, the number of diagnoses, the number of lab procedures and the time in the hospital. 
These all seem to be correlated, which is not suprising, as spending a lot of time in the hospital usually means that the patient has to undergo a lot procedures.

```{r, echo=FALSE}
#Create dummy data so it can be used in hierical clustering
age.dummy.data <- dummy.data.frame(as.data.frame(diabetes.data$age))
colnames(age.dummy.data) <- c('0-10', '10-20', '20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90', '90-100') 


gender.dummy.data <- dummy.data.frame(as.data.frame(diabetes.data$gender))
colnames(gender.dummy.data) <- c("Female", "Male","Unknown Gender")

payer.code.dummy.data <- dummy.data.frame(as.data.frame(diabetes.data$payer_code))
colnames(payer.code.dummy.data) <- c("Unknown Payer Code", "BC", "CH", "CM", "CP", "DM", "FR", "HM", "MC", "MD", "MP", "OG","OT","PO","SI", "SP", "UN", "WC")

race.dummy.data <- dummy.data.frame(as.data.frame(diabetes.data$race))
colnames(race.dummy.data) <- c("Unknown Race", "African American", "Asian", "Caucasian", "Hispanic", "Other")

total.dummy.data <- cbind(age.dummy.data, gender.dummy.data, payer.code.dummy.data, race.dummy.data)

#age.dummy.data <- cbind(age.dummy.data, readmission.dummy.data)



# Bind the class column to the existing dataframe
total.dummy.data <- cbind(total.dummy.data, diabetes.data$readmitted)
# Rename the class column
colnames(total.dummy.data)[38] <- "readmitted"
#total.dummy.data$readmitted <- combineLevels(total.dummy.data$readmitted, levs = c("<30", ">30"), newLabel = "YES")


# Write to an arff file, to be used in weka
foreign::write.arff(total.dummy.data[sample(nrow(total.dummy.data), 200),], file = "arfffiles/preWekaDummy.arff")
```


In the table below attribute description has been removed. This was done because the table would not fit on the page otherwise. The attribute description can be found in the link provided in the summary.


```{r, echo=FALSE, results='asis', message=FALSE}
diabetes.attribute.data$Description.and.values[1:28] = NA
knitr::kable(diabetes.attribute.data, caption = 'Table containing all attributes, attribute type, the percentage of missing values and the name used to descripe the values in the csv file.', format="latex", booktabs=TRUE) %>% 
  kableExtra::kable_styling(latex_options="scale_down")
```


# Missing data
Many of the weights are missing, this might have an impact on the readmission rates, but it holds no relevance to the research question asked in this paper. Therefore these missing values have no significant impact. Even though more than half of the payer code information is missing, this doesn't mean it is unusable. As mentioned before health care in the USA is not mandetory, and therefore it is likely that patients whith an unknown health insurer have no health care to begin with. This means that it would be unwise to remove this data.


# Preparing the data
It has been decided that all patients which have missing data in one or more of the three attributes will be completely removed from the dataset. This will be done because, in case of the payer code, a large portion of all data is unknown. This will have a large influence on the overall research, so it will be better to not include these patients.


# Quality Metrics
The machine learning algorithm used in this analysis must be precise, since it can be a matter of life and death whether the patient will be readmitted or not. The speed in this case is less important, since a patient can be readmitted even after 30 days have passed since the patients release from the hospital. False negatives are extreme unfavorable, since this means the algorithm will predict the patient will not have to return even though the patient should have been readmitted. False positives are less unfavorable, because once these patients return it can be easily determined it was unnecessary. True positives are, of course, very important as well since the algorithm will predict these patients will have to be readmitted, and this is indeed the case. True negavites are perhaps the least impactful of the bunch. In this case the algorithm will predict these patients will not have to return, and they will not return.

```{r, message=FALSE}
# Combine the readmitted values to a single factor
total.dummy.data$readmitted <- combineLevels(total.dummy.data$readmitted, levs = c("<30", ">30"), newLabel = "YES")
diabetes.data$readmitted <- combineLevels(diabetes.data$readmitted, levs = c("<30", ">30"), newLabel = "YES")

# Write to an arff file, to be used in weka
#write.to.file <- diabetes.data[sample(nrow(diabetes.data), 200),]
write.to.file <- diabetes.data[, c(3, 5, 10, 12:14, 16:18, 23, 26:27, 31:32, 34, 43, 48:50)]
foreign::write.arff(write.to.file, file = "arfffiles/preWekaNotDummyAll.arff")
```


```{r}
weka.output <- foreign::read.arff("WekaResults/WekaResults.arff")
```

# Weka Results
## Explorer
Several measurements using weka have been performed. However the results have proven to be inconclusive. As preperation the encounter_id, patient_nbr, admission_type_id, discharge_disposition_id, admission_source_id, diag_1, diag_2 and diag_3 have been removed. This is because even though it's numeric in the data file, it actually corresponds with non numeric types. Therefore these columns have been removed. The cleaned data was then loaded into R and transformed into dummy data using the NominaltoBinary filter in the explorer. The choice to use the explorer rather than the experimenter was made because the data barely contains numeric data, so it would have to be transformed into dummy data. This option is available in the explorer, but not in the experimenter. Firstly attribute selection was used, but the result were puzzling. Eight different algorithms were used; ZeroR, OneR, Naïve Bayes, Simple Logistic, SMO, IBk, J48 and Random Forest. However virtually no classifiers identified the same attributes. Not only that, due to the data containing almost only dummy data the classifiers, for example, only idenfied age[70-80], rather than age. Because of this no attribute is clearly more influential than the other and therfore no attributes can be removed.

## Experimenter
Using the previously created dummy data, several classifiers were run using CostSensitiveClassifier. However this data does not contain all attributes. This dataset only containts the age,gender,payer code and race attributes. These were the attributes which were believed to correlate with readmission rates. The algorithmes used with the CostSensitveMatrix are the same as the ones used in the explorer. These results also were inconclusive. No algorithm perform significantly better than the other ones. Not only that the accuracy is also very low, the highest correctly identified instances being only 55%. This means flipping a coin gives a simmilar result to the algorithm.  

```{r}
# Create empty dataframe, will be filled with the confusion matrix
confusion.matrix <- data.frame()

for (data in split(weka.output, weka.output$Key_Scheme_options)) {
 
  results <- data.frame()
 
  for (run in split(data, data$Key_Run)) {
   
    median.runs <- data.frame(
      TP = round(median(run$Num_true_positives)),
      TN = round(median(run$Num_true_negatives)),
      FP = round(median(run$Num_false_positives)),
      FN = round(median(run$Num_false_negatives)),
      Correctly_classified = mean(run$Percent_correct),
      Incorrectly_classified = mean(run$Percent_incorrect)
    )
   
    # create a dataframe with the results
    results <- rbind(results, median.runs)
  }
 
  final <- data.frame(matrix(apply(results[,1:4], MARGIN = 2, FUN = sum), ncol = ncol(median.runs[,1:4])), row.names = data$Key_Scheme_options[1])
  final <- cbind(final, median.runs[,5:6])
 
  col.names.final <- c("TP","TN","FP","FN","Correctly_classified", "incorrectly_classified")
 
  # change the colnames
  colnames(final) <- col.names.final
 
  # fill the confusion matrix
  confusion.matrix <- rbind(confusion.matrix, final)
}
confusion.matrix
```

The confusion matrix show a very low number of true positives, and a fairly high number of false negatives. The percentage correctly identified is also fairly low, with none of the algorithms being higher that 57.5%. The false positves have a very low amount, but false positives are not very important. This is because if the algorithm predicts the patients will have to return and the patients are readmissioned as a precaution it will eventually become clear the patients never had to be readmitted in the first place.


# Learning Curve
```{r}
learning.curve <- foreign::read.arff('WekaResults/LearningCurve.arff')
learning.curve.pre <- data.frame()


for (data in split(learning.curve, learning.curve$Key_Scheme_options)) {
 
  results.learning.curve <- data.frame()
 
  for (run in split(data, data$Key_Run)) {
   
    mean.runs <- data.frame(Percent_incorrect = mean(run$Percent_incorrect))
   
    # create a dataframe with the results
    results.learning.curve <- rbind(results.learning.curve, mean.runs)
  }
 
  final <- data.frame(matrix(apply(results.learning.curve, MARGIN = 2, FUN = mean), ncol = ncol(mean.runs)), row.names = data$Key_Scheme_options[1])
 
  col.names.final <- c("percent incorrect")
 
  # change the colnames
  colnames(final) <- col.names.final
 
  # fill the confusion matrix
  learning.curve.pre <- rbind(learning.curve.pre, final)
}
```


![ROC Curve No](/homes/rvandepol/Bachelor/Jaar_3/Thema/Log/bioinfthema9/WekaResults/ROCCurveNo.jpg)


![ROC Curve Yes](/homes/rvandepol/Bachelor/Jaar_3/Thema/Log/bioinfthema9/WekaResults/ROCCurveYes.jpg)


```{r, fig.cap="Figure depicting the learning curve. The x-axis depicts the percentage of data removed while the y-axis depicts the percentage of incorrectly determined instances. Naive Bayes was used as an algorithm in weka."}
ggplot(learning.curve.pre, aes(x = seq(10,90,10), y = learning.curve.pre$`percent incorrect`)) +
  geom_line() +
  geom_point() +
  labs(title = "Learning curve of the produced model", x = "Percentage of data removed", y = "Percentage incorrect")
```
      
