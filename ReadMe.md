# Log of Diabetes 130-US hospitals for years 1999-2008 Data Set exploration

The files in this repository were all used to explore the dataset provided by the article Impact of HbA1c Measurement on Hospital Readmission Rates: Analysis of 70,000 Clinical Database Patient Records.  
This file contains all the data exploration and explanations as to why certain decisions were made. It also contains a file names Paper.Rmd, though this file has deprecated since many thing have been done since the last adjustment.  
The most interesting file would be FinalReport.Rmd and FinalReport.pdf. These files contain all the latest information and findings concerning this project. Other then that it contains Log.rmd and Log.pdf.  
These files contain all the code used to create the figures and might be interesting to take a look at.
